**Code for generating sentiment (senti4sd) Commit messages**

Target research:
*"Understanding the relation between developer sentiment in commit messages and Fix-inducing changes"*

Target conf:
SANER ERA track

Details:
Senti4SD requires bash to execute sentiment analysis. The java code
1. extracts commits from repositories
2. lists only the message in groups of 100 rows in csv files
3. prints a csv that maps each commit (sha) to the file number and row number the commit's message is in
4. executes senti4sd on the message list files
5. the output files are similar to the input ones except the rows are not sorted (hence the mapping)
6. using the mapping each sha is mapped to a sentiment value 
7. sha to sentiment values are printed in a csv file