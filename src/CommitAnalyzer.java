import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CommitAnalyzer {
    public static final String RESULT_DIR = "../CommitAnalysisResults/";
    List<GHCommit> commits;

    public CommitAnalyzer() {
        commits = new ArrayList<>();
    }

    public void iterateCommits(Git git, Repository repository) throws IOException, GitAPIException {
        String treeName = "refs/heads/master";
        Iterable<RevCommit> revCommits = git.log().all().call();

        for(RevCommit revCommit : revCommits){
            GHCommit ghCommit = new GHCommit(revCommit);
            commits.add(ghCommit);
//            System.out.println(revCommit.name());
        }
    }

    private Comparator<? super GHCommit> commitDateComparator = new Comparator<GHCommit>() {
        @Override
        public int compare(GHCommit ghCommit1, GHCommit ghCommit2) {
            return ghCommit1.getDate().compareTo(ghCommit2.getDate());
        }
    };

    public void printCommitList(String filePath) throws FileNotFoundException {
        System.out.println("<Printing Commit Messages>");

        String rootProjectDir = RESULT_DIR + filePath;
        MyFileProcessor.checkOrCreateDirectory(rootProjectDir);
        String textListDir = rootProjectDir + "/textList";
        MyFileProcessor.checkOrCreateDirectory(textListDir);

        String shaMapFileName = rootProjectDir + "/shaMap.csv";
        PrintWriter csvShaMapWriter = new PrintWriter(shaMapFileName);
        String listFileName = textListDir + "/1.csv";
        PrintWriter csvTextWriter = new PrintWriter(listFileName);

        commits.sort(commitDateComparator);

        int count = 0;
        int index = 1;
        for(GHCommit commit : commits){
            csvTextWriter.println(commit.getMessage());
            csvShaMapWriter.println(commit.getSha() + "," + index + "," + count);

            count++;
            if(count==100) {
                count = 0;
                index++;
                listFileName = textListDir + "/" + index + ".csv";
                csvTextWriter.close();
                csvTextWriter = new PrintWriter(listFileName);
            }
        }

        csvTextWriter.close();
        csvShaMapWriter.close();
    }
}
