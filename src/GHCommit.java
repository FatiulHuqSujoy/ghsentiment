import org.eclipse.jgit.revwalk.RevCommit;

import java.util.*;

public class GHCommit {
    private String sha;
    private String message;
    private int sentiment;
    private Boolean isFIC, shouldBeConsidered;
    private RevCommit revCommit;

    public GHCommit(RevCommit commit) {
        revCommit = commit;
        sha = commit.getName();
        message = singleLine(commit.getFullMessage());
        isFIC = false;
        shouldBeConsidered = false;
    }

    private String singleLine(String fullMessage) {
        List<String> lines = Arrays.asList(fullMessage.split("\n"));
        String singleLine = "";
        for(String line : lines){
            if(!(line.endsWith("\\.") || line.endsWith("\\?") || line.endsWith("\\!")))
                line += ".";
            singleLine += line;
        }
        return singleLine;
    }

    public RevCommit getRevCommit() {
        return revCommit;
    }

    public String getSha() {
        return sha;
    }

    public Boolean isFIC() {
        return isFIC;
    }

    public void setFIC(Boolean FIC) {
        isFIC = FIC;
    }

    @Override
    public String toString() {
        return "GHCommit{" +
                "sha='" + sha + '\'' +
                ", message='" + revCommit.getShortMessage() + '\'' +
                '}';
    }

    public void setConsideration() {
        shouldBeConsidered = true;
    }

    public Boolean shouldBeConsidered(){
        return shouldBeConsidered;
    }

    public Date getDate(){
        Date date = revCommit.getCommitterIdent().getWhen();
        return date;
    }

    public String getMessage() {
        return message;
    }
}
