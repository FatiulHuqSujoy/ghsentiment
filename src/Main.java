import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {
    private Git git;
    private Repository repository;

    public static void main(String[] args) {
        Main main = new Main();
        main.execute();
    }

    private void execute() {
        for(String projectName : getProjects()){
            try {
                System.out.println("Analyzing project: " + projectName + "...");
                openProject(projectName);
//                analyzeCommits(projectName);
                executeSenti4SD(projectName);
                finalizeSentiment(projectName);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (GitAPIException e) {
                e.printStackTrace();
            }
        }
    }

    private void finalizeSentiment(String projectName) throws FileNotFoundException {
        SentimentFinalizer sentimentFinalizer = new SentimentFinalizer(projectName.split("/")[1]);
        sentimentFinalizer.populateFileShas();
//        sentimentFinalizer.printFileShas();
        sentimentFinalizer.mapSentimentToSha();
        sentimentFinalizer.printShaSentiment();
    }

    private void executeSenti4SD(String projectName) {
        Senti4SDAnalyzer senti4SDAnalyzer = new Senti4SDAnalyzer(projectName.split("/")[1]);
        senti4SDAnalyzer.runSentimentAnalysis();
    }

    private void analyzeCommits(String projectName) throws IOException, GitAPIException {
        CommitAnalyzer commitAnalyzer = new CommitAnalyzer();
        commitAnalyzer.iterateCommits(git, repository);
        commitAnalyzer.printCommitList(projectName.split("/")[1]);
    }

    private void openProject(String projectName) throws IOException, GitAPIException {
        GHProjectProcessor projectProcessor = new GHProjectProcessor();
        projectProcessor.openRepo(projectName);

        git = projectProcessor.getGit();
        repository = projectProcessor.getRepository();
    }

    private String[] getProjects() {
        return new String[]{
//                "google/guava"
                "mockito/mockito",
                "apache/commons-lang",
                "apache/hadoop",
                "elastic/elasticsearch",
                "spring-projects/spring-framework",
                "SeleniumHQ/selenium",
                "elastic/elasticsearch",
                "github/android",
                "netty/netty",
                "Bukkit/Bukkit",
                "clojure/clojure",
                "facebook/facebook-android-sdk",
                "JakeWharton/ActionBarSherlock",
                "nathanmarz/storm"
        };
    }
}
