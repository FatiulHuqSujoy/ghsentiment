import java.io.File;

public class MyFileProcessor {

    public static void checkOrCreateDirectory(String dir) {
        File folder = new File(dir);
        if(!folder.exists()){
            folder.mkdirs();
        }
    }
}
