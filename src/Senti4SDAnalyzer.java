import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class Senti4SDAnalyzer {
    private static final String SENTI_RESULT_DIR = "results/";
    private static final String COMMIT_RESULT_DIR = "../CommitAnalysisResults/";
    private static final String TEXT_LIST_DIR = "/textList/";
    private static final String SENTI_LIST_DIR = "/sentiList/";
    public static final String SENTI4SD_DIR = "../../Projects/Senti4SD/ClassificationTask/";
    public static final String SENTI4SD_SH = "classificationTask.sh";
    private String projectName;
    private String textFolderPath;

    public Senti4SDAnalyzer(String projectName) {
        this.projectName = projectName;
        textFolderPath = COMMIT_RESULT_DIR + projectName + TEXT_LIST_DIR;
    }

    public void runSentimentAnalysis(){
        System.out.println("<Executing Senti4SD>");

        File folder = new File(textFolderPath);
        for (File file : folder.listFiles()) {
            senti4sd(file.getName());
        }

        System.out.println();
    }

    private void senti4sd(String srcFileName) {
        MyFileProcessor.checkOrCreateDirectory(SENTI4SD_DIR + SENTI_RESULT_DIR + projectName + SENTI_LIST_DIR);
        String resultFileName = SENTI_RESULT_DIR + projectName + SENTI_LIST_DIR + srcFileName;

        ProcessBuilder processBuilder = new ProcessBuilder();
        String command = "sh " + SENTI4SD_DIR + SENTI4SD_SH + " " + textFolderPath + srcFileName + " " + resultFileName;
        processBuilder.command("bash", "-c", command);

        try {
            Process process = processBuilder.start();

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
//                System.out.println("> " + line);
            }

            int exitVal = process.waitFor();
            if (exitVal == 0) {
                System.out.print(".");
            } else {
                System.out.println(exitVal + " ??????");
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
