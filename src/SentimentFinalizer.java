import java.io.*;
import java.util.*;

public class SentimentFinalizer {
    private String projectName;
    public static final String RESULT_DIR = "../CommitAnalysisResults/";
    public static final String MAP_FILE_NAME = "/shaMap.csv";
    public static final String SENTI_MAP_FILE_NAME = "/sentiMap.csv";

    public static final String SENTI4SD_DIR = "../../Projects/Senti4SD/ClassificationTask/results/";
    private static final String SENTI_LIST_DIR = "/sentiList/";

    private Map<Integer, FileSha> fileShas;
    private Map<String, String> shaSentiments;

    public SentimentFinalizer(String projectName) {
        this.projectName = projectName;
        fileShas = new HashMap<>();
        shaSentiments = new HashMap<>();
    }

    public void populateFileShas(){
        System.out.println("<Extracting SHA Maps>");

        String filePath = RESULT_DIR + projectName + MAP_FILE_NAME;
        File csvFile = new File(filePath);

        String line = "";
        String cvsSplitBy = ",";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {
                String[] contents = line.split(cvsSplitBy);
                int index = Integer.parseInt(contents[1]);
                String sha = contents[0];
                int row = Integer.parseInt(contents[2]);

                addEntryToFileSha(index, sha, row);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addEntryToFileSha(int index, String sha, int row) {
        FileSha fileSha = fileShas.getOrDefault(index, new FileSha(index));
        fileSha.addEntry(row, sha);
        fileShas.put(index, fileSha);
    }

    public void printFileShas(){
        for(Map.Entry<Integer, FileSha> entry : fileShas.entrySet()){
            FileSha fileSha = entry.getValue();
            System.out.println(fileSha);
        }
    }

    public void mapSentimentToSha(){
        System.out.println("<Mapping Sentiment to SHA>");

        for(Map.Entry<Integer, FileSha> entry : fileShas.entrySet()){
            FileSha fileSha = entry.getValue();
            Map<String, String> rowShas = fileSha.rowToSha;

            String resultFileName = SENTI4SD_DIR + projectName + SENTI_LIST_DIR + fileSha.fileName;
            File sentiFile = new File(resultFileName);
            String line = "";
            try (BufferedReader br = new BufferedReader(new FileReader(sentiFile))) {

                while ((line = br.readLine()) != null) {
                    String contents[] = line.split(",");

                    if(contents[0].equals("Row"))
                        continue;

                    String row = contents[0];
                    String senti = contents[1];
                    String sha = rowShas.get(row);
                    shaSentiments.put(sha, senti);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void printShaSentiment() throws FileNotFoundException {
        System.out.println("<Printing Sentiment Result>");

        String filePath = RESULT_DIR + projectName + SENTI_MAP_FILE_NAME;
        PrintWriter csvSentiMapWriter = new PrintWriter(filePath);

        for(Map.Entry<String, String> entry : shaSentiments.entrySet()){
            csvSentiMapWriter.println(entry.getKey() + "," + entry.getValue());
        }

        csvSentiMapWriter.close();
    }
}

class FileSha{
    int index;
    String fileName;
    Map<String, String> rowToSha;

    public FileSha(int index) {
        this.index = index;
        fileName = index + ".csv";
        rowToSha = new TreeMap<>();
    }

    public void addEntry(int row, String sha){
        rowToSha.put("t"+row, sha);
    }

    @Override
    public String toString(){
        return fileName + ": " + rowToSha;
    }
}
